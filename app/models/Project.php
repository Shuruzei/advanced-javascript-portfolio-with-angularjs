<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Project extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $guarded = ['created_at', 'updated_at', 'deleted_at'];

	use SoftDeletingTrait;
}